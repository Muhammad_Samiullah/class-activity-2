package com.example.classactivity2;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class NextScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.next_screen);
    }
}